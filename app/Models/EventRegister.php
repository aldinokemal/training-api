<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventRegister extends Model
{
    use HasFactory;

    protected $primaryKey = 'evreg_id';
    protected $table = 'event_register';
    protected $guarded = ['evreg_id'];

	protected function serializeDate(DateTimeInterface $date)
	{
		return $date->format('Y-m-d H:i:s');
	}

	public function event()
	{
		return $this->belongsTo(Event::class, "evreg_event_id", "event_id");
	}
}
