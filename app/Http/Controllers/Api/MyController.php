<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MyController extends Controller
{
    public function data()
    {
        $data = [
            'user_id'  => auth('api')->id(),
            'email'    => auth('api')->user()->email,
            'picture'  => auth('api')->user()->picture,
            'fullname' => auth('api')->user()->fullname,
        ];

        return response()->json($data);
    }
}
