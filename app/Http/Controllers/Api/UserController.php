<?php

namespace App\Http\Controllers\Api;

use App\Events\RefreshUser;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

//use App\Http\Resources\UserCollection;

class UserController extends Controller
{

	public function index()
	{
		//$data = User::orderBy('user_id', 'desc')->paginate(10);
		//return new UserCollection($data);
		$data = User::orderBy('user_id', 'desc')->get();
		$data = UserResource::collection($data);
		return response()->json(['message' => 'data ditemukan', 'results' => $data]);
	}


	public function store(Request $request)
	{
		$faker = Factory::create();

		$validator = Validator::make($request->all(), [
			'email' => 'required|email|unique:App\Models\User,email',
			'password' => 'required|min:4',
			'picture' => 'sometimes|mimes:jpeg,bmp,png,jpg'
		]);

		if ($validator->fails()) {
			return response()->json(['message' => 'validation error', 'results' => $validator->getMessageBag()], 400);
		}

		$dataInsert = [
			"email" => $request->email,
			"password" => empty($request->pasword) ? bcrypt('1234') : bcrypt($request->password),
			"picture" => 'default.png',
			"gender" => empty($request->gender) ? 'male' : $request->gender,
			"fullname" => empty($request->fullname) ? $faker->name : $request->fullname,
		];

		if ($request->hasFile('picture')) {
			$picture = $request->file('picture');
			$picture_name = Str::random(10) . '_' . Str::slug(explode($picture->getClientOriginalExtension(), $picture->getClientOriginalName())[0]) . '.' . $picture->getClientOriginalExtension();
			$dataInsert['picture'] = $picture_name;

			$picture->move(public_path("images/profile"), $picture_name);
		}

		$processInsert = User::create($dataInsert);
		event(new RefreshUser());
		return response()->json(['message' => 'add sukses', 'results' => $processInsert]);
	}

	public function show(User $user)
	{
		$data = new UserResource($user);
		return response()->json(['message' => 'add sukses', 'results' => $data]);
	}


	public function update(Request $request, User $user)
	{
		$validator = Validator::make($request->all(), [
			'gender' => 'required',
			'fullname' => 'required',
			'picture' => 'sometimes|mimes:jpeg,bmp,png,jpg'
		]);
		if ($validator->fails()) {
			return response()->json(['message' => 'validation error', 'results' => $validator->getMessageBag()], 400);
		}

		$user->gender = $request->gender;
		$user->fullname = $request->fullname;

		if ($request->hasFile('picture')) {
			$picture = $request->file('picture');
			$picture_name = Str::random(10) . '_' . Str::slug(explode($picture->getClientOriginalExtension(), $picture->getClientOriginalName())[0]) . '.' . $picture->getClientOriginalExtension();
			$user->picture = $picture_name;

			$picture->move(public_path("images/profile"), $picture_name);
		}

		$user->save();
		event(new RefreshUser());
		return response()->json(['message' => 'update sukses', 'results' => $user]);
	}

	public function destroy(User $user)
	{
		$user->delete();
		event(new RefreshUser());
		return response()->json(['message' => 'delete sukses', 'results' => null]);
	}
}
