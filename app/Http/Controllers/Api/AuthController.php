<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Google_Client;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function processLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'validation error', 'results' => $validator->getMessageBag()], 400);
        }


        $credential = [
            'email'    => $request->email,
            'password' => $request->password
        ];
        $token      = Auth::guard('api')->attempt($credential);

        $data = [
            'user_id'  => auth('api')->id(),
            'email'    => auth('api')->user()->email,
            'picture'  => auth('api')->user()->picture,
            'fullname' => auth('api')->user()->fullname,
        ];

        if ($token) {
            $data = [
                'data_user'    => $data,
                'access_token' => $token,
                'token_type'   => 'bearer',
                'expires_in'   => Auth::guard('api')->factory()->getTTL()
            ];

            return response()->json([
                'message' => 'login success',
                'results' => $data
            ]);
        } else {
            return response()->json([
                'message' => 'login failed',
                'results' => null
            ], 400);
        }
    }

    public function processRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'required|email|unique:App\Models\User,email',
            'password' => 'required|min:4'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'validation error', 'results' => $validator->getMessageBag()], 400);
        }


        $faker = \Faker\Factory::create();

        $dataInsert = [
            "email"    => $request->email,
            "password" => empty($request->password) ? bcrypt('1234') : bcrypt($request->password),
            "picture"  => 'default.png',
            "gender"   => empty($request->gender) ? 'male' : $request->gender,
            "fullname" => empty($request->fullname) ? $faker->name : $request->fullname,
        ];

        try {
            $dataRegister = User::create($dataInsert);
            return response()->json(['message' => 'register sukses', 'results' => null]);
        } catch (QueryException $e) {
            return response()->json(['message' => "Email $request->email telah digunakan", 'results' => null], 500);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'results' => null], 500);
        }
    }

    public function oauthGoogleLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id_token' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => 'validation error', 'results' => $validator->getMessageBag()], 400);
        }
        try {
            $client  = new Google_Client();  // Specify the CLIENT_ID of the app that accesses the backend
            $payload = $client->verifyIdToken($request['id_token']);
            if ($payload) {
                $email    = $payload['email'];
                $fullname = $payload['name'];
                $image    = $payload['picture'];

                $faker = \Faker\Factory::create();

                $dataInsert = [
                    "email"    => $email,
                    "password" => empty($request->password) ? bcrypt('1234') : bcrypt($request->password),
                    "picture"  => empty($image) ? 'default.png' : $image,
                    "gender"   => empty($request->gender) ? 'male' : $request->gender,
                    "fullname" => empty($fullname) ? $faker->name : $fullname,
                ];

                try {
                    User::create($dataInsert);
                    return response()->json(['message' => 'register sukses', 'results' => null]);
                } catch (QueryException $e) {
                    return response()->json(['message' => "Email $request->email telah digunakan", 'results' => null], 500);
                } catch (\Exception $e) {
                    return response()->json(['message' => $e->getMessage(), 'results' => null], 500);
                }

            } else {
                return response()->json(['message' => "invalid token kak", 'results' => null], 400);
            }
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage(), 'results' => null], 400);
        }

    }

    public function oauthGoogleRegister(Request $request)
    {

    }

    public function oauthGoogleRegisterLogin(Request $request)
    {

    }
}
