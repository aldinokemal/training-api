<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventCollection;
use App\Models\Event;
use App\Models\EventRegister;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class EventController extends Controller
{

	public function __construct()
	{
		//$token =JWTAuth::getToken();
		//$apy = JWTAuth::getPayload($token)->toArray();;
		//dd($apy);
	}

	public function index()
	{
		$data = Event::orderBy('event_start', 'desc')->paginate(30);
		return new EventCollection($data);
	}


	public function register(Request $request, $event_id)
	{
		$data = [
			'evreg_user_id' => auth('api')->id(),
			'evreg_event_id' => $event_id,
			'evreg_lat' => $request->lat,
			'evreg_lng' => $request->lng,
		];

		try {
			EventRegister::create($data);
			return response()->json(['message' => 'register sukses', 'results' => null]);
		} catch (QueryException $e) {
			return response()->json(['message' => "Anda telah terdaftar dalam event ini", 'results' => null], 500);
		} catch (\Exception $e) {
			return response()->json(['message' => $e->getMessage(), 'results' => null], 500);
		}
	}

	public function myEvent()
	{
		$data = EventRegister::with("event")->where('evreg_user_id', auth('api')->id())->get();
		return response()->json($data);
	}
}
