<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
	public function toArray($request)
	{
		return [
			'event_id' => $this->event_id,
			'event_name' => $this->event_name,
			'event_picture' => $this->event_picture,
			'event_desc' => $this->event_desc,
			'event_is_close' => $this->event_is_close,
			'event_start' => (string) $this->event_start,
			'event_end' => (string) $this->event_end,
		];
	}
}
