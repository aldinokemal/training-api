<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'prev_page' => $resource->currentPage() > 1 ? $resource->path() . "?page=" . ($resource->currentPage() - 1) : NULL,
            'next_page' => $resource->currentPage() < $resource->lastPage() ? $resource->path() . "?page=" . ($resource->currentPage() + 1) : NULL,
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'message' => 'data found',
            'results' => UserResource::collection($this->collection),
            'pagination' => $this->pagination
        ];
    }
}
