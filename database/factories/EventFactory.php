<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $data = $this->faker->dateTimeBetween($startDate = '-1 years', $endDate = '+1 years', $timezone = 'Asia/Jakarta');
        return [
            'event_name' => $this->faker->city,
            'event_picture' => 'https://picsum.photos/id/' . array_rand(range(1, 200), 1) . '/200/300',
            'event_desc' => $this->faker->paragraph(),
            'event_start' => $data->format("Y-m-d 09:00:00"),
            'event_end' => $data->format("Y-m-d 12:00:00"),
        ];
    }
}
