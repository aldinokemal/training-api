<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $date = $this->faker->dateTimeBetween($startDate = '-40 years', $endDate = '-15 years', $timezone = 'Asia/Jakarta');
        return [
            'fullname' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => '$2y$10$DqPVCJqf0PB3YZit4Pa8MOrqT5Npx4jq.WllJsVJaSe5hBIPVZA96', // bcrypt('1234'),
            'gender' => $this->faker->randomElement(['male', 'female']),
            'birthday' => $date->format("Y-m-d"),
        ];
    }
}
