<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_register', function (Blueprint $table) {
	        $table->id("evreg_id");
	        $table->foreignId('evreg_user_id');
	        $table->foreignId('evreg_event_id');
	        $table->text('evreg_lat')->nullable();
	        $table->text('evreg_lng')->nullable();
	        $table->timestamps();

	        $table->foreign('evreg_user_id')->references('user_id')->on('user')->onDelete('cascade');
	        $table->foreign('evreg_event_id')->references('event_id')->on('event')->onDelete('cascade');

	        $table->unique(['evreg_user_id', 'evreg_event_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_register');
    }
}
