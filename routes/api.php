<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\EventController;
use App\Http\Controllers\Api\MyController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('events')->middleware('auth.api')->group(function () {
	Route::get('/', [EventController::class, 'index']);
	Route::post('/register/{event_id}', [EventController::class, 'register']);
	Route::get('/me', [EventController::class, 'myEvent']);
});

Route::apiResource('users', UserController::class);

Route::post('oauth/google/login', [AuthController::class, 'oauthGoogleLogin']);
Route::post('oauth/google/register', [AuthController::class, 'oauthGoogleRegister']);
Route::post('oauth/google/register-login', [AuthController::class, 'oauthGoogleRegisterLogin']);

Route::post('auth/login', [AuthController::class, 'processLogin']);
Route::post('auth/register', [AuthController::class, 'processRegister']);

Route::prefix('my')->middleware('auth.api')->group(function() {
   Route::get('/data', [MyController::class, 'data']);
});
